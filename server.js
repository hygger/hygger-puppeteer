const puppeteer = require('puppeteer');
const args = require('minimist')(process.argv);
global.atob = require('atob');

(async () => {
    const authHost = getAuthHost();
    const appHost = getAppHost();
    const userConfig = getUserConfig();
    const boardConfig = getBoardConfig();

    boardConfig.paperFormat = args['paper-format'] ? args['paper-format'] : boardConfig.paperFormat;
    console.log(boardConfig);
    const viewportSize = getViewportSize(boardConfig.paperFormat, boardConfig.isLandscape);

    const path = (args['env'] ? '/tmp/' : 'pdf_results/') + (args['file-name'] || Date.now()) + '.pdf';
    // const path = '/storage/www/hygger-puppeteer/pdf_results/' + (args['file-name'] || Date.now()) + '.pdf';

    const browser = await puppeteer.launch({headless: true, ignoreHTTPSErrors: true, args: ['--no-sandbox', '--disable-setuid-sandbox']});
    const page = await browser.newPage();
    await page.setViewport(viewportSize);
    await page.goto(authHost);
    await page.waitForSelector('#mail');
    await page.type('#mail', userConfig.userMail);
    await page.type('#pass', userConfig.tempPass);
    await page.click('[a-button].btn_primary');
    await page.waitForNavigation({waitUntil: 'networkidle2'}).then(()=>console.log('Login Success')).catch(e => console.log(e));
    await page.goto(appHost).then(()=>console.log('Go To Host Success')).catch(e => console.log(e));
    await page.evaluate((boardConfig)=>{
        const guiState = {
            roadmapZoomMode: {[boardConfig.boardId]: boardConfig.scale}
        };
        localStorage.setItem('guiStateMemorized-' + boardConfig.userId, JSON.stringify(guiState));
        localStorage.setItem(boardConfig.storageKey, boardConfig.filterValue);
    }, boardConfig).then(()=>console.log('Set Local Storage Success')).catch(e => console.log(e));
    await page.goto(appHost + '/b/pdf-export/' + boardConfig.boardId).then(()=>console.log('Go To Board Success')).catch(e => console.log(e));
    await page.waitForSelector('#roadmap-paint-finished');
    await page.pdf({
        path: path,
        width: viewportSize.width,
        height: viewportSize.height
    });


    await browser.close();
})();

/**
 * A number, or a string containing a number.
 * @typedef {Object} BoardConfig
 * @property {number} boardId - Border ID
 * @property {string} storageKey - Key for local storage
 * @property {string} filterValue - Value of filter in local storage
 * @property {string} paperFormat - Format of printing page
 * @property {boolean} isLandscape - Use Landscape if true
 */


/**
 * Get link to auth page based on script argument 'env'
 * @returns {string}
 */
function getAuthHost() {
    switch (args['env']) {
        case 'prod': {
            return 'https://accounts.hygger.io';
        }
        case 'stage': {
            return 'https://accounts.atlaz-stage.com';
        }
        default: {
            return 'http://localhost:4200';
        }
    }
}

/**
 * Get link to app page based on script argument 'env'
 * @returns {string}
 */
function getAppHost() {
    switch (args['env']) {
        case 'prod': {
            return 'https://' + args['company-domain'] + '.hygger.io';
        }
        case 'stage': {
            return 'https://' + args['company-domain'] + '.atlaz-stage.com';
        }
        default: {
            return 'http://localhost:3002';
        }
    }
}

/**
 * Get user e-mail and temporary password as object from script arguments
 * @returns {{userMail: string, tempPass: string}}
 */
function getUserConfig() {
    return {
        userMail: args['mail'],
        tempPass: args['pass'] + ''
    }
}

/**
 * Get board config from args
 * @returns  {BoardConfig}
 */
function getBoardConfig() {
    return JSON.parse(atob(args['board-config']));
}


function getViewportSize(format, isLandscape) {
    //default for A4 portrait
    // 1 mm = 96 / 25,4 (dpi = 96)
    const result = {
        width: 794,
        height: 1122,
        deviceScaleFactor: 1
    };

    switch (format){
        case 'tabloid': {
            result.width = 1632;
            result.height = 1056;
            break;
        }
        case 'ledger': {
            result.width = 1056;
            result.height = 1632;
            break;
        }
        case 'letter': {
            result.width = 816;
            result.height = 1056;
            break;
        }
        case 'legal': {
            result.width = 816;
            result.height = 1344;
            break;
        }
        case 'A3': {
            result.width = 1122;
            result.height = 1588;
            break;
        }
        case 'A2': {
            result.width = 1588;
            result.height = 2245;
            break;
        }
        case 'A1': {
            result.width = 2245;
            result.height = 3178;
            break;
        }
        case 'A0': {
            result.width = 3178;
            result.height = 4493;
            break;
        }
        default: {
            const t = format.split('x');
            if(parseInt(t[0]) && parseInt(t[1])){
                result.width = parseInt(t[0]);
                result.height = parseInt(t[1]);
                isLandscape = false;
            }
            break;
        }
    }
    if(isLandscape){
        const w = result.height;
        result.height = result.width;
        result.width = w;
    }
    return result;
}